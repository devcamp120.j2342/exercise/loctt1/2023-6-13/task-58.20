package comdevcamp.s50.restapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import comdevcamp.s50.restapi.models.CDrink;
import comdevcamp.s50.restapi.reponsitory.IDrinkRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CDrinkController {
     @Autowired
     IDrinkRepository pCustomerResponsory;

     @GetMapping("/drinks")
     public ResponseEntity<List<CDrink>> getAllCDrink(){
          try{
               List<CDrink> listCustomer = new ArrayList<CDrink>();
               
              pCustomerResponsory.findAll()
              .forEach(listCustomer::add);
              return new ResponseEntity<>(listCustomer, HttpStatus.OK);
          }catch(Exception ex){
               return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
          }
     }
}
