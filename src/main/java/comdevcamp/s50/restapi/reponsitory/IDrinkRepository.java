package comdevcamp.s50.restapi.reponsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import comdevcamp.s50.restapi.models.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long>{
     
}
